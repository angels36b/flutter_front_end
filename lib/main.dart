import 'package:flutter/material.dart';
import 'package:proyecto_flutter/src/pages/Home_page.dart';
import 'src/pages/Home_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Componente',
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}
